FROM node as build
ENV NODE_ENV=production
COPY . /workdir
WORKDIR /workdir
RUN npm install
RUN npm run build

FROM node as runtime
COPY --from=build /workdir/.next /app/.next
COPY --from=build /workdir/node_modules /node_modules
ENTRYPOINT [ "/node_modules/next/dist/bin/next", "start", "/app"]
